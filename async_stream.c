#include <time.h>
#include <stdint.h>
#include <string.h>

#ifdef linux
    #include <stdlib.h>
    #include <pthread.h>
    #include <stdio.h>

    void sleep_posix(int32_t millisecs)
    {
        struct timespec duration;
        ts.tv_sec = 0;
        ts.tv_nsec = millisecs * 1000;
        nanosleep(&duration, NULL);
    }
#endif

#ifdef _WIN32
    #include <stdio.h>
    #include <windows.h>
#endif

char *_filename_shared, *_mode_shared;
int32_t _chunksize_shared, _wait_time;

char *_result;
int32_t _result_ready; // 1 ready, 0 dont access
int64_t _result_size;

char *_read_data_chunks()
{
    FILE *in;
    int64_t fsize;
    char *buf;
    int64_t bytes_done,bytes_left;

    //Get Size of file
    in = fopen(_filename_shared, _mode_shared);

    if(in == NULL)
    {
        fprintf(stderr, "Failed to open file %s with mode %s\n", _filename_shared, _mode_shared);
        return NULL;
    }
    fseek(in, 0L, SEEK_END);
    fsize = ftell(in);
    rewind(in);

    buf = malloc((fsize)*sizeof(char));

    if(buf == NULL)
    {
        fprintf(stderr, "Failed to allocate memory for file\n");
        return NULL;
    }

    printf("filesize %.6f Mb\n", (float)fsize/1000000);
    //Load file without sleeping thread
    if(_chunksize_shared == -1 || _chunksize_shared > fsize)
    {
        printf("load file complete\n");
        fread(buf, sizeof(char)*fsize, sizeof(char), in);
        fclose(in);

        if(strstr("r", _mode_shared))
            buf[fsize-sizeof(char)] = '\0';

        _result = buf;
        _result_ready = 1;
        _result_size = fsize;
    }
    else //Load chunks with sleep
    {
        bytes_done = 0;
        bytes_left = fsize;

        while(bytes_left > 0)
        {
            printf("%.2f done loading.\n", ((float)bytes_done / (float)fsize)*100);
            if(bytes_left >= _chunksize_shared)
            {
                fread(buf + bytes_done, _chunksize_shared, sizeof(char), in);
                bytes_done += _chunksize_shared;
                bytes_left -= _chunksize_shared;

                #ifdef linux
                    sleep_posix(_wait_time);
                #elif _WIN32
                    Sleep(_wait_time);
                #endif
            }
            else
            {
                fread(buf+bytes_done, bytes_left, sizeof(char), in);
                bytes_done += fsize-bytes_done;
                bytes_left = 0;
                if(strstr("r", _mode_shared))
                    buf[fsize-sizeof(char)] = '\0';

                _result = buf;
                _result_ready = 1;
                _result_size = fsize;
            }
        }
    }
}

char *async_load(char *filename, char *mode, int32_t chunksize,int32_t waittime)
{
    #ifdef _WIN32
    HANDLE thread;
    _filename_shared = filename;
    _mode_shared = mode;
    _chunksize_shared = chunksize;
    _wait_time = waittime;

    thread = CreateThread(NULL,0, _read_data_chunks, NULL, 0, NULL);
    if(NULL == thread)
    {
        fprintf(stderr, "Failed to create thread, returning!\n");
        return NULL;
    }
    WaitForSingleObject(thread, INFINITE);
    #endif // _WIN32

    #ifdef linux
    pthread_t thread;
    int32_t tret;
    tret = pthread_create(&thread, NULL, _read_data_chunks, NULL);
    if(iret1)
    {
        fprintf(stderr,"Failed to create Posix Thread\n");
        return NULL;
    }
    #endif // linux

    if(_result_ready == 1)
    {
        return _result;
    }
    else
    {
        return NULL;
    }
}


