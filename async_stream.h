#ifndef ASYNC_STREAM_H_
#define ASYNC_STREAM_H_

#include <stdint.h>

extern char *_filename_shared, *_mode_shared;
extern int32_t _chunksize_shared, _wait_time;

extern char *_result;
extern int32_t _result_ready; // 1 ready, 0 dont access
extern int64_t _result_size;

char *_read_data_chunks(const char *filename, const char *mode, int32_t kbSec, int32_t waittime);
char *_read_data_chunks();
#endif // ASYNC_STREAM_H_INCLUDED
