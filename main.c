#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "async_stream.h"

int main()
{
    clock_t b;
    clock_t e;

    b = clock();

    int *datn = async_load("Test.dat", "rb", 5, 200);
    printf("size: %d for test.dat\n", _result_size);

    char *datc = async_load("Test2.txt", "r",1000000, 500);
    printf("size: %d for test2.txt\n", _result_size);
    e = clock();
    printf("Time in milliseconds: %d\n", e-b);

    printf("%d %d %d %d %d\n",
           datn[0],datn[1],
           datn[2],datn[3],
           datn[4]);
    printf("%s\n", datc);


    getchar();
    return 0;
}
